import { NgModule } from '@angular/core'
import { RouterModule, Routes, PreloadAllModules } from '@angular/router'
import { GameRulesComponent } from './containers/games-wiki/game-rules/game-rules.component'
import { GamesListComponent } from './containers/games-wiki/games-list/games-list.component'

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'games-list',
    component: GamesListComponent
  },
  {
    path: 'game-rules',
    component: GameRulesComponent
  },
  {
    path: 'auth',
    loadChildren: './core/auth/auth.module#AuthModule'
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
