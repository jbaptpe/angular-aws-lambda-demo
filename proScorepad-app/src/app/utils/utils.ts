import pickBy from 'lodash-es/pickBy'
import * as jwtDecode from 'jwt-decode'

// extract array from observable
export function unwrapObservable(observableObj): any {
  if (observableObj === undefined) { return [] }
  let finalList: any
  observableObj.forEach((element: any) => {
    finalList = element
  })
  return finalList
}

export function getIdTokenValue() {
  const idTokenRegex = new RegExp ('idToken')
  let idToken: string

  pickBy(localStorage, (value, key) => {
    if (key.match(idTokenRegex)) {
      return idToken = value
    }
  })

  return idToken !== undefined ? jwtDecode(idToken) : null
}
