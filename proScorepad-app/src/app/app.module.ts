import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module'
import { HttpClientModule } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

// Redux
import { StoreModule } from './state-management/store/store.module'

// Modules
import { MaterialModule } from './material.module'
import { UsersFeaturesModule } from './containers/users-features/users-features.module';

// Components
import { AppComponent } from './app.component'
import { MainNavComponent } from './containers/main-nav/main-nav.component'
import { LayoutModule } from '@angular/cdk/layout'
import { MainContentComponent } from './containers/main-content/main-content.component'
import { SidebarComponent } from './containers/main-nav/sidebar/sidebar.component'
import { UsersComponent } from './containers/main-nav/sidebar/users/users.component'
import { FooterComponent } from './containers/main-nav/sidebar/footer/footer.component'
import { LinksComponent } from './containers/main-nav/sidebar/links/links.component'
import { GameRulesComponent } from './containers/games-wiki/game-rules/game-rules.component'
import { GamesListComponent } from './containers/games-wiki/games-list/games-list.component'

// Services
import { AuthService } from './core/services/auth.service'
import { MongoUserService } from './core/services/mongo-user.service'
import { GamesService } from './core/services/games.service'
import { UserGamesService } from './core/services/user-games.service'
import { DialogService } from './core/services/mat-dialog.service'
import { PagerService } from './core/services/pager.service'
import { PaginationModule } from './containers/pagination/pagination.module';

@NgModule({
  declarations: [
    AppComponent,
    MainNavComponent,
    MainContentComponent,
    SidebarComponent,
    UsersComponent,
    FooterComponent,
    LinksComponent,
    GameRulesComponent,
    GamesListComponent
  ],
  imports: [
    StoreModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    LayoutModule,
    MaterialModule,
    UsersFeaturesModule,
    PaginationModule
  ],
  providers: [
    MongoUserService,
    GamesService,
    AuthService,
    DialogService,
    UserGamesService,
    PagerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
