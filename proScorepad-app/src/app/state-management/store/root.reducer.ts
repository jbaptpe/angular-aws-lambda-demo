/*
 Created on Dec 05, 2016

 Copyright (c) 2017 Dealflo Limited. All Rights Reserved.

 This software is the proprietary information of Dealflo Limited.
 All rights reserved.

 This software is the confidential and proprietary information of Dealflo Limited.
 You shall not disclose such Confidential Information and
 shall use it only in accordance with the terms of the license agreement you
 entered into with Dealflo Limited.
 */

import { combineReducers } from 'redux'
import { createAuthReducer } from '../../core/auth/auth.reducer'
import { createGamesReducer } from '../../containers/users-features/games.reducer'

// Define the global store shape by combining our application's
// reducers together into a given structure.
export const rootReducer = combineReducers({
    auth: createAuthReducer(),
    games: createGamesReducer()
})
