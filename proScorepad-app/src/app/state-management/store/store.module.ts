/*
 Created on Dec 05, 2016

 Copyright (c) 2017 Dealflo Limited. All Rights Reserved.

 This software is the proprietary information of Dealflo Limited.
 All rights reserved.

 This software is the confidential and proprietary information of Dealflo Limited.
 You shall not disclose such Confidential Information and
 shall use it only in accordance with the terms of the license agreement you
 entered into with Dealflo Limited.
 */
import { NgModule } from '@angular/core'
import { AuthActions } from '../../core/auth/auth.actions'
import { GamesActions } from '../../containers/users-features/games.actions'

// Angular-redux ecosystem stuff.
// @angular-redux/form and @angular-redux/router are optional
// extensions that sync form and route location state between
// our store and Angular.
import { NgReduxModule, NgRedux, DevToolsExtension } from '@angular-redux/store'
import { NgReduxRouterModule } from '@angular-redux/router'

// The top-level reducers and epics that make up our app's logic.
import { AppState } from './root.types'
import { rootReducer } from './root.reducer'

@NgModule({
  imports: [NgReduxModule, NgReduxRouterModule],
  providers: [
    AuthActions,
    GamesActions
  ],
})
export class StoreModule {
  constructor(
    public store: NgRedux<AppState>,
    private devTools: DevToolsExtension
  ) {
    store.configureStore(
      rootReducer,
      {},
      [],
      devTools.isEnabled() ? [devTools.enhancer()] : []
    )
  }
}
