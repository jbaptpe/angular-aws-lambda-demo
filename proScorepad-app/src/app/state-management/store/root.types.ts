import { IAuthModule } from '../../core/auth/auth.types'
import { IGamesModule } from '../../containers/users-features/games.types'

export interface AppState {
  auth?: IAuthModule
  userGames?: IGamesModule
}
