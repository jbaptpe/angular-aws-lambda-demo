import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent {
  @Input() pager
  @Output() setPage = new EventEmitter<number>()

  pagination(page: number) {
    this.setPage.emit(page)
  }
}
