import { Component, OnInit, OnDestroy } from '@angular/core'
import { select } from '@angular-redux/store'
import { Subscription, Observable } from 'rxjs'
import { AuthService } from '../../../../core/services/auth.service'

@Component({
  selector: 'app-sidebar-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, OnDestroy {
  @select(['auth', 'mongoUser']) mongoUser$: Observable<object>
  user: object
  connectedUser: boolean
  mongoUserStateChange: Subscription

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.mongoUserStateChange = this.mongoUser$.subscribe(user => {
      this.user = user
      this.connectedUser = Object.keys(user).length > 0 ? true : false
    })
  }

  ngOnDestroy() {
    this.mongoUserStateChange.unsubscribe()
  }

  logout() {
    this.authService.signOut()
  }
}
