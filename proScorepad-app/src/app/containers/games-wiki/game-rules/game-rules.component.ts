import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { select } from '@angular-redux/store'
import { Subscription, Observable } from 'rxjs'
import { GamesService } from '../../../core/services/games.service'

@Component({
  selector: 'app-game-rules',
  templateUrl: './game-rules.component.html',
  styleUrls: ['./game-rules.component.scss']
})
export class GameRulesComponent implements OnInit {
  @select(['auth', 'mongoUser']) mongoUser$: Observable<object>
  connectedUser: boolean
  mongoUserStateChange: Subscription
  game

  constructor(
    private gamesService: GamesService,
    private route: ActivatedRoute) {}

  ngOnInit() {
    this.mongoUserStateChange = this.mongoUser$.subscribe(user => {
      this.connectedUser = Object.keys(user).length > 0 ? true : false
    })

    const gameName = this.route.snapshot.queryParams.gameName
    this.gamesService.getSingleGame(gameName).subscribe(result => this.game = result[0])
  }
}
