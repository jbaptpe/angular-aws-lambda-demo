import { Component, OnInit } from '@angular/core'
import { GamesService } from '../../../core/services/games.service'
import { PagerService } from '../../../core/services/pager.service'
import { Router } from '@angular/router'
import { select } from '@angular-redux/store'
import { Subscription, Observable, Subject } from 'rxjs'
import { debounceTime } from 'rxjs/operators'


@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.scss']
})
export class GamesListComponent implements OnInit {
  @select(['auth', 'mongoUser']) mongoUser$: Observable<object>
  connectedUser: boolean
  subject: Subject<string> = new Subject()
  mongoUserStateChange: Subscription
  gamesList: any
  searchResult: object[]
  pager: any = {}
  pagedItems: any[]
  gameName: string
  userSearched = false

  constructor(
    private gamesService: GamesService,
    private pagerService: PagerService,
    private router: Router) { }

  ngOnInit() {
    this.mongoUserStateChange = this.mongoUser$.subscribe(user => {
      this.connectedUser = Object.keys(user).length > 0 ? true : false
    })

    this.gameServiceCall()

    this.subject.pipe(
      debounceTime(500)
    ).subscribe(gameName => this.gameServiceCall(gameName))
  }

  getGames() {
    this.userSearched = true
    this.subject.next(this.gameName)
  }

  gameServiceCall(name?) {
    this.gamesService.getGames('20', name).subscribe(names =>  {
      this.gamesList = names
      this.setPage(1)
    })
  }

  openGamePage(name) {
    this.router.navigate(['game-rules'], { queryParams: { gameName: name }})
  }

  setPage(page: number) {
    this.pager = this.pagerService.getPager(this.gamesList['games'].length, page)
    this.pagedItems = this.gamesList['games'].slice(this.pager.startIndex, this.pager.endIndex + 1)
  }
}
