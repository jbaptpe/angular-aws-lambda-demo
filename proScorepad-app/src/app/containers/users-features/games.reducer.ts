import { GamesActions } from './games.actions'
import { IGamesModule } from './games.types'
import cloneDeep from 'lodash-es/cloneDeep'

export const INITIAL_STATE: IGamesModule = {
  userGames: []
}

export function createGamesReducer() {
  return function gamesReducer(state = INITIAL_STATE, action) {
    switch (action.type) {
      case GamesActions.SET_USER_GAMES:
        return Object.assign({}, state, { userGames: action.payload })

      case GamesActions.UPDATE_USER_GAME:
        const updatedUserGamesState = cloneDeep(state)
        console.log(action.payload)

        updatedUserGamesState.userGames.map((game, key) => {
          if (game['_id'] === action.payload._id) {
            updatedUserGamesState.userGames[key] = action.payload
          }
        })

        return Object.assign({}, state, updatedUserGamesState)

      case GamesActions.ADD_USER_GAME:
        const addedGameState: IGamesModule = cloneDeep(state)
        addedGameState.userGames.push(action.payload)

        return Object.assign({}, state, addedGameState)

      // To create later, when we need help to remove a game
      case GamesActions.REMOVE_USER_GAME:
        const removedGameState = cloneDeep(state)

        return state

      default:
        return state
    }
  }
}
