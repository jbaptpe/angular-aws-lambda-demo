import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core'
import { GamesService } from '../../../core/services/games.service'
import { GamesActions } from '../games.actions'
import { MongoUserService } from '../../../core/services/mongo-user.service'
import { UserGamesService } from '../../../core/services/user-games.service'
import { select } from '@angular-redux/store'
import { Subscription, Observable } from 'rxjs'
import remove from 'lodash-es/remove'
import forEach from 'lodash-es/forEach'
import cloneDeep from 'lodash-es/cloneDeep'
import find from 'lodash-es/find'

@Component({
  selector: 'app-create-game',
  templateUrl: './create-user-game.component.html',
  styleUrls: ['./create-user-game.component.scss']
})
export class CreateUserGameComponent implements OnInit, OnDestroy {
  @select(['auth', 'mongoUser']) mongoUser$: Observable<object>
  @ViewChild('userInput', { static: true }) userInput: ElementRef
  gameName: string
  playerName: string
  gamesSuggestions: string[]
  selectedGame: string
  playersSuggestions: string[] = []
  partners: object[] = []
  user
  mongoUserStateChange: Subscription
  errorMessage:  string

  constructor(
    private gameService: GamesService,
    private mongoUserService: MongoUserService,
    private userGamesService: UserGamesService,
    private gamesActions: GamesActions
  ) { }

  ngOnInit() {
    this.mongoUserStateChange = this.mongoUser$.subscribe(user => {
      this.user = user
    })
  }

  ngOnDestroy() {
    this.mongoUserStateChange.unsubscribe()
  }

  getGames() {
    if (this.gameName.length > 1) {
      this.gameService.getGamesSuggestions(this.gameName).subscribe(names =>  this.gamesSuggestions = names)
    }
  }

  nameDisplay(value?): string | undefined {
    return value ? value.name : undefined
  }

  defineSelectedGame(game) {
    this.selectedGame = game.id
  }

  getUsers() {
    if (this.playerName.length > 1) {
      this.mongoUserService.getUsernamesCollection(this.playerName).subscribe(players => {
        // Remove already added players
        players.forEach((player) => {
          const isCurrentUser = player.id === this.user._id
          const isAlreadySuggested = find(this.playersSuggestions, ['name',  player.name])
          const isAlreadyAPartner = find(this.partners, ['name',  player.name])

          if (!isCurrentUser &&  isAlreadyAPartner === undefined && isAlreadySuggested === undefined) {
            this.playersSuggestions.push(player)
          }
        })
      })
    }
  }

  addPartner(partner) {
    Object.assign(partner, { score: 0})
    this.partners.push(partner)
    this.userInput.nativeElement.value = ''
    this.playersSuggestions = []
  }

  removePartner(name) {
    remove(this.partners, { name })
  }

  createGame() {
    if (this.partners.length === 0) {
      this.errorMessage = 'You need to add friends to your game!'
      return
    } else if (this.errorMessage) {
      this.errorMessage = null
    }

    if (!this.selectedGame) {
      this.errorMessage = 'You need to select a game.'
      return
    }

    const creator = {
      id: this.user._id,
      score: 0
    }
    const partnersCleaned = forEach(cloneDeep(this.partners), (element) => delete element['name'])

    this.userGamesService.createNewGame(this.selectedGame, creator, partnersCleaned).subscribe(game => {
      this.gamesActions.addUserGame(game)
    })
  }
}
