import { Component, OnInit, OnDestroy } from '@angular/core'
import { select } from '@angular-redux/store'
import { Subscription, Observable } from 'rxjs'
import { AuthService } from '../../../core/services/auth.service'

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit, OnDestroy {
  @select(['auth', 'mongoUser']) mongoUser$: Observable<object>
  mongoUserStateChange: Subscription
  connectedUser
  user
  editMode = false

  constructor(private authService: AuthService) {}

  ngOnInit() {
    this.mongoUserStateChange = this.mongoUser$.subscribe(user => {
      this.user = user
      this.connectedUser = Object.keys(user).length > 0 ? true : false
    })
  }

  ngOnDestroy() {
    this.mongoUserStateChange.unsubscribe()
  }

  toggleEditMode() {
    this.editMode = !this.editMode
  }

  updateInfos() {
    const updateAttributes = {
      preferred_username: this.user.preferred_username,
      email: this.user.email
    }
    this.authService.updateUser(updateAttributes)
  }
}
