import { Component, OnInit, OnDestroy } from '@angular/core'
import { Router } from '@angular/router'
import { PagerService } from '../../../core/services/pager.service'
import { select } from '@angular-redux/store'
import { Subscription, Observable } from 'rxjs'

@Component({
  selector: 'app-user-games',
  templateUrl: './user-games.component.html',
  styleUrls: ['./user-games.component.scss']
})
export class UserGamesComponent implements OnInit, OnDestroy {
  @select(['auth', 'mongoUser']) mongoUser$: Observable<object>
  @select(['games', 'userGames']) userGames$: Observable<object>
  userGames
  user: object
  mongoUserStateChange: Subscription
  userGamesStateChange: Subscription
  totalGames: string
  pager: any = {}
  pagedItems: any[]

  constructor(
    private pagerService: PagerService,
    private router: Router) { }

  ngOnInit() {
    this.mongoUserStateChange = this.mongoUser$.subscribe(user => {
      this.user = user
    })

    this.userGamesStateChange = this.userGames$.subscribe(games => {
      this.userGames = Object.keys(games).length > 0 ? games : null
      if (this.userGames !== null) {
        this.setPage(1)
        this.totalGames = this.userGames.length === 1 ? '1' : 'other'
      }
    })
  }

  openGamePage(id) {
    this.router.navigate(['user-game'], { queryParams: { gameId: id }})
  }

  ngOnDestroy() {
    this.userGamesStateChange.unsubscribe()
    this.mongoUserStateChange.unsubscribe()
  }

  setPage(page: number) {
    this.pager = this.pagerService.getPager(this.userGames.length, page)
    this.pagedItems = this.userGames.slice(this.pager.startIndex, this.pager.endIndex + 1)
  }
}
