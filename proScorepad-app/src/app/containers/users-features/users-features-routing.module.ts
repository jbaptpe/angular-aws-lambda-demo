import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { UserGamesComponent } from './user-games/user-games.component'
import { UserGameComponent } from './user-game/user-game.component'
import { CreateUserGameComponent } from './create-user-game/create-user-game.component'
import { UserProfileComponent } from './user-profile/user-profile.component'
import { CreateTemplateGameComponent } from './create-template-game/create-template-game.component'
import { AuthGuard } from 'src/app/auth/auth.guard'

const routes: Routes = [
  {
    path: 'create-user-game',
    component: CreateUserGameComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'create-game-template',
    component: CreateTemplateGameComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'user-games',
    component: UserGamesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'user-game',
    component: UserGameComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'user-profile',
    component: UserProfileComponent,
    canActivate: [AuthGuard]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersFeaturesRoutingModule { }
