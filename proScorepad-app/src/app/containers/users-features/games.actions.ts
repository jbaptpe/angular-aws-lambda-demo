import { Injectable } from '@angular/core'
import { dispatch } from '@angular-redux/store'

@Injectable()
export class GamesActions {
  static readonly SET_USER_GAMES = 'SET_USER_GAMES'
  static readonly UPDATE_USER_GAME = 'UPDATE_USER_GAME'
  static readonly REMOVE_USER_GAME = 'REMOVE_USER_GAME'
  static readonly ADD_USER_GAME = 'ADD_USER_GAME'

  @dispatch()
  addUserGame = (action) => ({
    type: GamesActions.ADD_USER_GAME,
    meta: { status: 'started' },
    payload: action
  })

  @dispatch()
  removeUserGame = (action) => ({
    type: GamesActions.REMOVE_USER_GAME,
    meta: { status: 'started' },
    payload: action
  })

  @dispatch()
  setUserGames = (action) => ({
    type: GamesActions.SET_USER_GAMES,
    meta: { status: 'started' },
    payload: action
  })

  @dispatch()
  updateUserGame = (action) => ({
    type: GamesActions.UPDATE_USER_GAME,
    meta: { status: 'started'},
    payload: action
  })
}
