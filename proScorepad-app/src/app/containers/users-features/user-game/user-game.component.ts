import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { UserGamesService } from '../../../core/services/user-games.service'
import { select } from '@angular-redux/store'
import { Subscription, Observable } from 'rxjs'
import { GamesActions } from '../games.actions'
import find from 'lodash-es/find'

export interface GameUpdate {
  creatorScore: number
  partners: number[]
}

@Component({
  selector: 'app-game',
  templateUrl: './user-game.component.html',
  styleUrls: ['./user-game.component.scss']
})
export class UserGameComponent implements OnInit {
  @select(['games', 'userGames']) userGames$: Observable<object>
  userGames
  game
  gameUpdate: GameUpdate = {
    creatorScore: 0,
    partners: []
  }
  updateMode = false
  userGamesStateChange: Subscription


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userGamesService: UserGamesService,
    private gamesActions: GamesActions) { }

  ngOnInit() {
    const gameId = this.route.snapshot.queryParams.gameId
    this.userGamesStateChange = this.userGames$.subscribe(games => {
      this.userGames = Object.keys(games).length > 0 ? games : null
    })
    this.game = find(this.userGames, { _id: gameId })
  }

  setFirstGameUpdate() {
    this.gameUpdate.creatorScore = this.game.creator.score
    this.game.partners.forEach(partner => this.gameUpdate.partners.push(partner.score))
  }

  updateCreatorScore(score) {
    this.gameUpdate.creatorScore = score
  }

  updatePartnerScore(score, index) {
    this.gameUpdate.partners[index] = score
  }

  updateScores() {
    this.userGamesService.updateUserGame(this.game._id, this.gameUpdate).subscribe(game => {
      this.gamesActions.updateUserGame(game)
      this.router.navigate(['user-games'])
    })
  }

  toggleUpdateMode() {
    this.updateMode = !this.updateMode
  }
}
