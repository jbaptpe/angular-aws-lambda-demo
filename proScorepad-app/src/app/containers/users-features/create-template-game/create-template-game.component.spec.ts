import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { CreateTemplateGameComponent } from './create-template-game.component'

describe('CreateTemplateGameComponent', () => {
  let component: CreateTemplateGameComponent
  let fixture: ComponentFixture<CreateTemplateGameComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTemplateGameComponent ]
    })
    .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTemplateGameComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
