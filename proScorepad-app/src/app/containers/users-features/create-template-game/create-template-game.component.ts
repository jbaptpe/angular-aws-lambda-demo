import { Component, OnInit } from '@angular/core'
import { GamesService } from 'src/app/core/services/games.service'

export class GameTemplate {
  name: string
  description: string
  rules: string
  origin?: string
}

@Component({
  selector: 'app-create-template-game',
  templateUrl: './create-template-game.component.html',
  styleUrls: ['./create-template-game.component.scss']
})
export class CreateTemplateGameComponent implements OnInit {
  gameTemplate: GameTemplate

  constructor(private gamesService: GamesService) { }

  ngOnInit() {
    this.gameTemplate = new GameTemplate()
  }

  createGame() {
    this.gamesService.createGame(this.gameTemplate).subscribe()
  }
}
