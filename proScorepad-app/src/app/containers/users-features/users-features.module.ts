import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { CreateTemplateGameComponent } from './create-template-game/create-template-game.component'
import { CreateUserGameComponent } from './create-user-game/create-user-game.component'
import { UserGameComponent } from './user-game/user-game.component'
import { UserGamesComponent } from './user-games/user-games.component'
import { UserProfileComponent } from './user-profile/user-profile.component'
import { UsersFeaturesRoutingModule } from './users-features-routing.module'
import { MaterialModule } from 'src/app/material.module'
import { PaginationModule } from '../pagination/pagination.module'

@NgModule({
  declarations: [
    CreateTemplateGameComponent,
    CreateUserGameComponent,
    UserGameComponent,
    UserGamesComponent,
    UserProfileComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    UsersFeaturesRoutingModule,
    PaginationModule
  ]
})
export class UsersFeaturesModule { }
