import { Injectable } from '@angular/core'
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router'
import { Observable } from 'rxjs'
import { AuthService } from '../core/services/auth.service'
import { select } from '@angular-redux/store';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  @select(['auth', 'mongoUser']) mongoUser$: Observable<object>

  constructor(private authService: AuthService, private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const url: string = state.url
    return this.checkLogin(url)
  }

  checkLogin(url: string): boolean {
    if (this.authService.isLoggedIn) { return true }

    this.authService.redirectUrl = url
    this.router.navigate(['/auth/login'], { queryParams: {trigger: 'unauthorized-access'}})
    return false
  }
}
