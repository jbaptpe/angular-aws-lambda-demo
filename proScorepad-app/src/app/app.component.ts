import { Component, OnInit } from '@angular/core'
import { getIdTokenValue } from './utils/utils'
import { MongoUserService } from './core/services/mongo-user.service'
import { AuthService } from './core/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'app'

  constructor(private mongoUserService: MongoUserService,
              private authService: AuthService) {}

  ngOnInit() {
    const idToken = getIdTokenValue()
    if (idToken !== null) {
      this.authService.isLoggedIn = true
      this.mongoUserService.getUser(idToken['preferred_username'], true).subscribe()
    }
  }
}
