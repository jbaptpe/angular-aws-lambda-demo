import { Component } from '@angular/core'
import { DialogService } from '../../services/mat-dialog.service'
import { CognitoCallback, AuthService } from '../../services/auth.service'
import { Router} from '@angular/router'
import { LoginComponent } from '../login/login.component'
import { RegisterComponent } from '../register/register.component'
@Component({
  selector: 'app-resend',
  templateUrl: './resend-confirmation.component.html',
  styleUrls: ['./resend-confirmation.component.scss']
})
export class ResendConfirmationComponent implements CognitoCallback {
  emailText: string
  errorMessage: string

  constructor(
    public authService: AuthService,
    public router: Router,
    private dialogService: DialogService
  ) {}

  resendCode() {
    this.authService.resendConfirmation(this.emailText, this)
  }

  cognitoCallback(error: any, result: any) {
    if (error != null) {
      this.errorMessage = 'Something went wrong...please try again'
    } else {
      this.router.navigate(['/home/confirmRegistration', this.emailText])
    }
  }

  openDialog(cp) {
    const component = cp === 'register' ? RegisterComponent : LoginComponent
    this.dialogService.openDialog(component)
  }
}
