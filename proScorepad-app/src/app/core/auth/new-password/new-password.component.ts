import { Component, OnInit } from '@angular/core'
import { Router} from '@angular/router'
import { CognitoCallback, AuthService } from '../../services/auth.service'

export class NewPassword {
  existingPassword: string
  password: string
}
/**
* This component is responsible for displaying and controlling
* the registration of the user.
*/
@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.scss']
})
export class NewPasswordComponent implements CognitoCallback, OnInit {
  registrationUser: NewPassword
  errorMessage: string

  constructor(
    public authService: AuthService,
    private router: Router
  ) {
    this.onInit()
  }

  onInit() {
    this.registrationUser = new NewPassword()
    this.errorMessage = null
  }

  ngOnInit() {
    this.errorMessage = null
    console.log('Checking if the user is already authenticated. If so, then redirect to the secure site')
    // this.userService.isAuthenticated(this);
  }

  onRegister() {
    console.log(this.registrationUser)
    this.errorMessage = null
    this.authService.newPassword(this.registrationUser, this)
  }

  cognitoCallback(message: string, result: any) {
    if (message != null) { // error
      this.errorMessage = message
      console.log('result: ' + this.errorMessage)
    } else { // success
      // move to the next step
      console.log('redirecting')
      this.router.navigate(['/securehome'])
    }
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (isLoggedIn) {
      this.router.navigate(['/securehome'])
    }
  }
}
