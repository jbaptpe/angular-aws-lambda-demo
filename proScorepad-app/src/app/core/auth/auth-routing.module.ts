import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'

import { AuthComponent } from './auth.component'
import { RegisterComponent } from './register/register.component'
import { NewPasswordComponent } from './new-password/new-password.component'
import { ForgotPasswordStep1Component, ForgotPasswordStep2Component } from './forgot-password/forgot-password.component'
import { LoginComponent } from './login/login.component'
import { ResendConfirmationComponent } from './resend-confirmation/resend-confirmation.component'

const routes: Routes = [
  { path: '', component: AuthComponent, pathMatch: 'full' },
  { path: 'new-password', component: NewPasswordComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'forgot-password-1', component: ForgotPasswordStep1Component },
  { path: 'forgot-password-2/:email', component: ForgotPasswordStep2Component },
  { path: 'login', component: LoginComponent },
  { path: 'resend-confirmation', component: ResendConfirmationComponent }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
