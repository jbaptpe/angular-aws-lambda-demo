import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { CognitoCallback, LoggedInCallback, AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements CognitoCallback, LoggedInCallback, OnInit {
  emailText: string
  password: string
  errorMessage: string
  trigger: string
  mfaStep = false
  mfaData = {
    destination: '',
    callback: null
  }
  userUnconfirmed = false

  constructor(
    public route: ActivatedRoute,
    public router: Router,
    private authService: AuthService) {}

  ngOnInit() {
    this.errorMessage = null
    this.route.queryParams.subscribe(params => {
      this.trigger = params['trigger']
    })
  }

  onLogin() {
    if (this.emailText == null || this.password == null) {
      this.errorMessage = 'All fields are required'
      return
    }
    this.errorMessage = null
    this.authService.signIn(this.emailText, this.password, this) // Introduce callback logic
  }

  cognitoCallback(message: string, result: any) {
    if (message != null) { // error
      this.errorMessage = message
      if (this.errorMessage === 'User is not confirmed.') {
        this.userUnconfirmed = true
      } else if (this.errorMessage === 'User needs to set password.') {
        this.router.navigate(['auth', 'new-password'])
      }
    } else { // success
      this.router.navigate(['/'])
    }
  }

  resendConfirmation() {
    this.authService.resendConfirmation(this.emailText, this)
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
    if (isLoggedIn) {
      this.router.navigate(['/'])
    }
  }
}
