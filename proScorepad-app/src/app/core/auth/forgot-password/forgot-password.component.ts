import { Component, OnDestroy, OnInit, Inject } from '@angular/core'
import { ActivatedRoute, Router} from '@angular/router'
import { CognitoCallback, AuthService } from '../../services/auth.service'

export interface DialogData {
  email: string
}

@Component({
  selector: 'app-forgot-password-step-1',
  templateUrl: './forgot-password-step-1.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordStep1Component implements CognitoCallback {
  emailText: string
  errorMessage: string

  constructor(
    private authService: AuthService,
    public router: Router) {
      this.errorMessage = null
    }

  onNext() {
    this.errorMessage = null
    this.authService.forgotPassword(this.emailText, this)
  }

  cognitoCallback(message: string, result: any) {
    console.log('cognitoCallback has been called', message, result)
    if (message == null && result == null) {
      this.router.navigate(['auth', 'forgot-password-2', this.emailText])
    } else {
      this.errorMessage = message
    }
  }
}

@Component({
  selector: 'app-forgot-password-step-2',
  templateUrl: './forgot-password-step-2.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordStep2Component implements CognitoCallback, OnInit, OnDestroy {
  verificationCodeText: string
  email: string
  password: string
  errorMessage: string
  private sub: any

  constructor(
    private authService: AuthService,
    public router: Router,
    public route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.email = params['email']
    })
    this.errorMessage = null
  }

  ngOnDestroy() {
    this.sub.unsubscribe()
  }

  onNext() {
    this.errorMessage = null
    this.authService.forgotPasswordSubmit(this.email, this.verificationCodeText, this.password, this)
  }

  cognitoCallback(message: string) {
    if (message != null) {
      this.errorMessage = message
    } else {
      this.router.navigateByUrl('/auth/login')
    }
  }
}
