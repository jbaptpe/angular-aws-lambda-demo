import { AuthActions } from './auth.actions'
import { IAuthModule } from './auth.types'

export const INITIAL_STATE: IAuthModule = {
  mongoUser: {}
}

export function createAuthReducer() {
  return function authReducer (state = INITIAL_STATE, action) {
    switch (action.type) {
      case AuthActions.SET_MONGO_USER:
        return Object.assign({}, state, { mongoUser: action.payload })
      case AuthActions.RESET_MONGO_USER:
        return Object.assign({}, state, { mongoUser: {}})
      default:
        return state
    }
  }
}

