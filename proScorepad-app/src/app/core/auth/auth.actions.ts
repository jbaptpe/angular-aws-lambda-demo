import { Injectable } from '@angular/core'
import { dispatch } from '@angular-redux/store'

@Injectable()
export class AuthActions {
  static readonly SET_MONGO_USER = 'SET_MONGO_USER'
  static readonly RESET_MONGO_USER = 'RESET_MONGO_USER'

  @dispatch()
  setMongoUser = (action) => ({
    type: AuthActions.SET_MONGO_USER,
    meta: { status: 'started'},
    payload: action
  })

  @dispatch()
  resetMongoUser = () => ({
    type: AuthActions.RESET_MONGO_USER,
    meta: { status: 'started' }
  })
}
