import { CommonModule } from '@angular/common'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { AuthRoutingModule } from './auth-routing.module'

// Component
import { RegisterComponent } from './register/register.component'
import { NewPasswordComponent } from './new-password/new-password.component'
import { ForgotPasswordStep1Component, ForgotPasswordStep2Component } from './forgot-password/forgot-password.component'
import { LoginComponent } from './login/login.component'
import { ResendConfirmationComponent } from './resend-confirmation/resend-confirmation.component'
import { AuthComponent } from './auth.component'

import { MaterialModule } from '../../material.module'

@NgModule({
  declarations: [
    RegisterComponent,
    NewPasswordComponent,
    ForgotPasswordStep1Component,
    ForgotPasswordStep2Component,
    LoginComponent,
    ResendConfirmationComponent,
    AuthComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AuthRoutingModule,
    MaterialModule
  ],
  exports: [
    LoginComponent
  ]
})
export class AuthModule { }
