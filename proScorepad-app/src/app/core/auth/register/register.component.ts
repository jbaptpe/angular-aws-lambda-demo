import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { MongoUserService } from '../../services/mongo-user.service'
import { CognitoCallback, AuthService } from '../../services/auth.service'
import { IAuthModule } from '../auth.types'
import { AuthActions } from '../auth.actions'
import { Observable } from 'rxjs'

export class RegistrationUser {
  preferred_username: string
  email: string
  phone_number: string
  password: string
}

export class MongoUser {
  _id: string
  preferred_username: string
  alias: string
  email: string
  playedGames: Array<string>
}
/**
* This component is responsible for displaying and controlling
* the registration of the user.
*/
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']

})
export class RegisterComponent implements CognitoCallback, OnInit {
  registrationUser: RegistrationUser
  router: Router
  errorMessage: string
  auth: Observable<IAuthModule>

  constructor(
    private userService: MongoUserService,
    private authActions: AuthActions,
    private authService: AuthService,
    router: Router) {
    this.router = router
  }

  ngOnInit() {
    this.registrationUser = new RegistrationUser()
    this.errorMessage = null
  }

  onRegister() {
    this.errorMessage = null
    this.userService.createUser(this.registrationUser, this)
      .subscribe((data) => {
        // Dispatch set action redux
        this.authActions.setMongoUser(data)
        this.authService.signUp(data, this.registrationUser, this)
      })
  }

  cognitoCallback(message: string, result: any) {
    if (message != null) {
      this.errorMessage = message
      console.log('result: ' + this.errorMessage)
    } else {
      this.router.navigate(['/user-profile/'])
    }
  }
}
