import { Injectable } from '@angular/core'
import { MatDialog } from '@angular/material'

@Injectable()
export class DialogService {
  constructor(private dialog: MatDialog) {}

  openDialog(component, additionalData?) {
    this.closeAllDialogs()

    this.dialog.open(component, {
      width: '50vw',
      maxWidth: '600px',
      autoFocus: true,
      closeOnNavigation: true,
      data: additionalData || {}
    })
  }

  closeAllDialogs() {
    this.dialog.closeAll()
  }
}
