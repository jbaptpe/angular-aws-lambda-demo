import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { map, catchError} from 'rxjs/operators'
import { throwError } from 'rxjs'
import { environment } from '../../../environments/environment'
import { GamesActions } from '../../containers/users-features/games.actions'

@Injectable()
export class UserGamesService {
  playedGamesApiGateway = `${environment.apiGateway}${environment.env}/played-games`

  constructor(
    private http: HttpClient,
    private gamesActions: GamesActions) { }

  createNewGame(game, creator, partners) {
    const requestHeaders = new HttpHeaders().set('Content-Type', 'application/json')
    const requestBody = { game, creator, partners }

    return this.http.put(this.playedGamesApiGateway, requestBody, { headers: requestHeaders })
      .pipe(
        map((res: any) => {
          return res
        }),
        catchError((error: any) =>  {
          return throwError(error)
        })
      )
  }

  getUserGames(userId) {
    let params = new HttpParams()
    params = params.append('id', userId)
    params = params.append('type', 'getUserGames')

    return this.http.get(this.playedGamesApiGateway, { params: params })
      .pipe(
        map((games: any) => {
          this.gamesActions.setUserGames(games)
          return games
        }),
        catchError((error: any) =>  {
          return throwError(error)
        })
      )
  }

  getSingleGame(gameId) {
    let params = new HttpParams()
    params = params.append('type', 'getSingleGame')
    params = params.append('id', gameId)

    return this.http.get(this.playedGamesApiGateway, { params: params })
      .pipe(
        map((game: any) => {
          return game
        }),
        catchError((error: any) =>  {
          return throwError(error)
        })
      )
  }

  updateUserGame(id, update) {
    const requestHeaders = new HttpHeaders().set('Content-Type', 'application/json')
    const requestBody = { id, update }

    return this.http.post(this.playedGamesApiGateway, requestBody, { headers: requestHeaders })
      .pipe(
        map((res: any) => {
          return res
        }),
        catchError((error: any) =>  {
          return throwError(error)
        })
      )
  }
}
