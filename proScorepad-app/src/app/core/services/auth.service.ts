import { Injectable } from '@angular/core'
import { environment } from '../../../environments/environment'
import Amplify from '@aws-amplify/core'
import Auth from '@aws-amplify/auth'
import { AuthActions } from '../auth/auth.actions'
import { MongoUserService } from './mongo-user.service'
import { NewPassword } from '../auth/new-password/new-password.component'
import { Router } from '@angular/router';

Amplify.configure({
  Auth: {
    region: environment.region,
    userPoolId: environment.userPoolId,
    userPoolWebClientId: environment.clientId
  }
})

export interface CognitoCallback {
  cognitoCallback(message: string, result: any): void
}

export interface LoggedInCallback {
  isLoggedIn(message: string, loggedIn: boolean): void
}

export interface Callback {
  callback(): void

  callbackWithParam(result: any): void
}

@Injectable()
export class AuthService {
  isLoggedIn = false
  redirectUrl = './user-profile'

  constructor(
    private authActions: AuthActions,
    private mongoUserService: MongoUserService,
    private router: Router) { }

  signUp(mongoUser, registrationUser, component) {
    Auth.signUp({
      'username': mongoUser.preferred_username,
      'password': registrationUser.password,
      'attributes': {
        'email': mongoUser.email,
        'custom:mongo_id': mongoUser._id,
        'preferred_username': mongoUser.preferred_username
      }
    })
    .then(data => this.router.navigate([this.redirectUrl]))
    .catch(err => console.log(err.message))
  }

  resendConfirmation(user, component) {
    Auth.resendSignUp(user)
      .then(data => console.log(data))
      .catch(err => component.cognitoCallback(err.message))
  }

  signOut() {
    Auth.signOut()
      .then(data => {
        this.authActions.resetMongoUser()
        this.isLoggedIn = false
        this.router.navigate(['/auth/login'], { queryParams: {trigger: 'logged-out'}})
      })
      .catch(err => console.log(err))
  }

  signIn(username, password, component) {
    this.signOut()

    Auth.signIn(username, password)
      .then(user => {
        this.mongoUserService.getUser(user.username, true).subscribe()
        this.isLoggedIn = true
        this.router.navigate([this.redirectUrl])
      })
      .catch(err => {
        if (err.code === 'UserNotConfirmedException') {
          component.cognitoCallback('User is not confirmed.', null)
        }
      })
  }

  forgotPassword(username, component) {
    Auth.forgotPassword(username)
      .then(data => component.cognitoCallback(null, null))
      .catch(err => component.cognitoCallback(err.message, null))
  }

  forgotPasswordSubmit(username, code, new_password, component) {
    Auth.forgotPasswordSubmit(username, code, new_password)
      .then(data => component.cognitoCallback(null))
      .catch(err => component.cognitoCallback(err.message))
  }

  updateUser(attributes) {
    Auth.currentAuthenticatedUser()
      .then(user => Auth.updateUserAttributes(user, attributes))
      .then(data => console.log(data))
      .catch(err => console.log(err.message))
  }

  newPassword(passwords: NewPassword, component) {
    Auth.currentAuthenticatedUser()
      .then(user => Auth.changePassword(user, passwords.existingPassword, passwords.password))
      .then(data => console.log(data))
      .catch(err => console.log(err.message))
  }
}
