import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { map, catchError} from 'rxjs/operators'
import { throwError } from 'rxjs'
import { environment } from '../../../environments/environment'

@Injectable()
export class GamesService {
  gamesApiGateway = `${environment.apiGateway}${environment.env}/games`

  constructor(private http: HttpClient) { }

  createGame(body) {
    const requestHeaders = new HttpHeaders().set('Content-Type', 'application/json')

    return this.http.put(this.gamesApiGateway, body, { headers: requestHeaders})
      .pipe(
        map((res: any) => {
          return res
        }),
        catchError((error: any) =>  {
          return throwError(error)
        })
      )
  }

  getGamesSuggestions(condition) {
    let params = new HttpParams()
    params = params.append('regex', condition)
    params = params.append('type', 'getSuggestions')

    return this.http.get(this.gamesApiGateway, { params: params })
      .pipe(
        map((res: any) => {
          return res
        }),
        catchError((error: any) =>  {
          return throwError(error)
        })
      )
  }

  getGames(quantity: string, name?: string) {
    let params = new HttpParams()
    params = params.append('type', 'getNames')
    params = params.append('limit', quantity ? quantity : '20')
    params = params.append('name', name ? name : '')
    console.log(params)

    return this.http.get(this.gamesApiGateway, { params: params })
      .pipe(
        map(res => res),
        catchError((error: any) =>  {
          return throwError(error)
        })
      )
  }

  getSingleGame(name) {
    let params = new HttpParams()
    params = params.append('type', 'getSingleGame')
    params = params.append('name', name)

    return this.http.get(this.gamesApiGateway, { params: params })
      .pipe(
        map((res: any) => {
          return res
        }),
        catchError((error: any) =>  {
          return throwError(error)
        })
      )
  }
}
