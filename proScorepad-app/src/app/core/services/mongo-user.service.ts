import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { map, catchError} from 'rxjs/operators'
import { throwError } from 'rxjs'
import { environment } from '../../../environments/environment'
import { CognitoCallback } from './auth.service'
import { AuthActions } from '../auth/auth.actions'
import { UserGamesService } from './user-games.service'

@Injectable()
export class MongoUserService {
  userApiGateway = `${environment.apiGateway}${environment.env}/user`

  constructor(
    private http: HttpClient,
    private authActions: AuthActions,
    private userGamesService: UserGamesService) {}

  createUser(userInfos, callback: CognitoCallback) {
    const requestHeaders = new HttpHeaders().set('Content-Type', 'application/json')
    const requestBody = {
      preferred_username: userInfos.preferred_username,
      email: userInfos.email
    }

    return this.http.put(this.userApiGateway, requestBody, { headers: requestHeaders})
      .pipe(
        map((res: any) => {
          return res
        }),
        catchError((error: any) =>  {
          callback.cognitoCallback(error.error, null)
          return throwError(error)
        })
      )
  }

  getUser(username, currentUser) {
    let params = new HttpParams()
    params = params.append('preferred_username', username)
    params = params.append('type', 'getSingleUser')

    return this.http.get(this.userApiGateway, { params: params })
      .pipe(
        map((user: any) => {
          if (currentUser) {
            this.userGamesService.getUserGames(user[0]._id).subscribe()
            this.authActions.setMongoUser(user[0])
          }
          return user
        }),
        catchError((error: any) =>  {
          return throwError(error)
        })
      )
  }

  getUsernamesCollection(regex) {
    let params = new HttpParams()
    params = params.append('regex', regex)
    params = params.append('type', 'getUsernamesCollection')

    return this.http.get(this.userApiGateway, { params: params })
      .pipe(
        map((res: any) => {
          return res
        }),
        catchError((error: any) =>  {
          return throwError(error)
        })
      )
  }

  deleteUser() {

  }

  updateUser() {

  }
}
