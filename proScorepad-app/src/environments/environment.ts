export const environment = {
  production: false,
  region: "XXX",
  userPoolId: "XXXX",
  clientId: "XXXX",
  cognitoIdpEndpoint: "",
  cognito_identity_endpoint: "",
  apiGateway: "XXXX",
  env: "dev",
};
