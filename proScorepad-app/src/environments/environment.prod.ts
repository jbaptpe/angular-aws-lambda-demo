export const environment = {
  production: true,
  region: "XXX",
  userPoolId: "XXX",
  clientId: "XXXX",
  cognitoIdpEndpoint: "",
  cognito_identity_endpoint: "",
  apiGateway: "XXXX",
  env: "dev",
};
