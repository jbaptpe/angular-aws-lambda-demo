const { src, series } = require('gulp')
const run = require('gulp-run')
const AWS = require('aws-sdk')
const tslint = require('gulp-tslint')
const gulpif = require('gulp-if')
const debug = require('gulp-debug')
const cached = require('gulp-cached')
const s3 = new AWS.S3({apiVersion: '2006-03-01'})


function buildAngular(done) {
  run('yarn build').exec()
  done()
}

function copyToBucket() {
  const pathToS3Bucket = "s3://proscorepad.com"
  const publishPackageCommand = "aws s3 cp ./dist/myScorePad-app " + pathToS3Bucket + " --recursive"

  return run(publishPackageCommand).exec()
}


function tsLint() {
  return src(['./src/**/*.ts'])
      .pipe(gulpif(global.isWatching, cached('tslint')))
      .pipe(debug({title: 'ts-lint: '}))
      .pipe(tslint({
        fix: true,
        formatter: 'prose',
        configuration: './tslint.json'
      }))
      .pipe(tslint.report({
        emitError: true
      }))
}

exports.tsLint = tsLint
exports.copyToBucket = copyToBucket
exports.buildAngular = buildAngular
exports.zipContent = zipContent
exports.cleanPublic = cleanPublic
exports.deploy = series(buildAngular, zipContent, cleanPublic, copyToBucket)
