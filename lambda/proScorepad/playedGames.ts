import { APIGatewayEvent, Callback, Context, Handler } from 'aws-lambda';
import { connectToDatabase, defaultResponseHeader } from './db/utils';

const PlayedGame = require('./db/models/playedGame');
const Game = require('./db/models/game');
const User = require('./db/models/user');

export const create: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const currentDate = Date.now();
  const game = Object.assign({}, JSON.parse(event.body), { created: currentDate, updated: currentDate});

  connectToDatabase().then(() => {
    PlayedGame.create(game)
      .then(game => {
        PlayedGame.findById(game._id)
          .populate({ path: 'game', select: 'name', model: Game })
          .populate({ path: 'creator.id', select: 'preferred_username', model: User })
          .populate({ path: 'partners.id', select: 'preferred_username', model: User })
          .then(result => cb(null, {
            statusCode: 200,
            headers: defaultResponseHeader,
            body: JSON.stringify(result)
          }))
      })
      .catch(err => cb(null, {
        statusCode: err.statusCode || 500,
        headers: { 'Content-Type': 'text/plain' },
        body: err
      }));
  });
};

// Need to get the user names as well (eventually instead of their id)
export const get: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const params: any = event.queryStringParameters;

  connectToDatabase().then(() => {
    if (params.type === 'getUserGames') {
      PlayedGame.find({ $or: [ {'creator.id': params.id}, { 'partners.id': params.id} ]})
      .populate({ path: 'game', select: 'name', model: Game })
      .populate({ path: 'creator.id', select: 'preferred_username', model: User })
      .populate({ path: 'partners.id', select: 'preferred_username', model: User })
      .then(games => {
        cb(null, {
        statusCode: 200,
        headers: defaultResponseHeader,
        body: JSON.stringify(games)
      })})
      .catch(err => {
        cb(null, {
          statusCode: err.statusCode || 500,
          headers: { 'Content-Type': 'text/plain' },
          body: err
        })
      });
    } else if (params.type === 'getAllGames') {
      PlayedGame.find({})
      .then(games => cb(null, {
        statusCode: 200,
        headers: defaultResponseHeader,
        body: JSON.stringify(games)
      }))
      .catch(err => cb(null, {
        statusCode: err.statusCode || 500,
        headers: { 'Content-Type': 'text/plain' },
        body: err
      }));
    } else if (params.type === 'getSingleGame') {
      PlayedGame.findById(params.id)
      .populate({ path: 'game', select: 'name', model: Game })
      .populate({ path: 'creator.id', select: 'preferred_username', model: User })
      .populate({ path: 'partners.id', select: 'preferred_username', model: User })
      .then(games => cb(null, {
        statusCode: 200,
        headers: defaultResponseHeader,
        body: JSON.stringify(games)
      }))
      .catch(err => cb(null, {
        statusCode: err.statusCode || 500,
        headers: { 'Content-Type': 'text/plain' },
        body: err
      }));
    }
  });
};

export const update: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const currentDate = Date.now();
  const body = JSON.parse(event.body);
  const update = body.update;

  connectToDatabase().then(() => {
    PlayedGame.findById(body.id)
      .then(game => {
        game.set('creator.score',  update.creatorScore);
        game.set('updated', currentDate);
        update.partners.forEach((score, index) => game.set(`partners.${index}.score`, score));

        game.save()
          .then(result => {
            cb(null, {
              statusCode: 200,
              headers: defaultResponseHeader,
              body: JSON.stringify(result)
            })
          })
          .catch(err => {
            cb(null, {
            statusCode: err.statusCode || 500,
            headers: { 'Content-Type': 'text/plain' },
            body: JSON.stringify({ 'Update failed: ': err })
          })});
        })
      .catch(err => {
        cb(null, {
        statusCode: err.statusCode || 500,
        headers: { 'Content-Type': 'text/plain' },
        body: JSON.stringify({ 'Document was not found: ': err })
      })});
  });
}
