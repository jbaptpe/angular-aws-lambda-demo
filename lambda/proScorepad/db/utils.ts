// Mongoose utils
export const mongoose = require('mongoose')
export const Schema = mongoose.Schema;

mongoose.Promise = global.Promise;
let isConnected: boolean = false;

export const connectToDatabase = () => {
  if (isConnected) {
    console.log('using existing database connection');
    return Promise.resolve();
  } else {
    console.log('using new database connection', { isConnected });
    return mongoose.connect(encodeURI(`mongodb://${process.env.MONGODB_USER}:${process.env.MONGODB_PWD}@${process.env.MONGODB_URL}`)).then(db => {
      isConnected = true;
    });
  }
};

// Cors utils
export const defaultResponseHeader = {
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Credentials': true,
};

