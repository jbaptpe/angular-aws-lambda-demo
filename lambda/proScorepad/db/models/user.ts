import { mongoose, Schema } from '../utils';
const PlayedGameSchema = require('./playedGame');

const userSchema = new Schema({
  preferred_username: String,
  created: Date,
  updated: Date,
  email: String,
  playedGames: [{
    type: Schema.Types.ObjectId,
    ref: 'PlayedGame'
  }]
});
module.exports = mongoose.model('User', userSchema);
