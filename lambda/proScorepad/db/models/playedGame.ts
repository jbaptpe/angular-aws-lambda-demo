import { mongoose, Schema } from '../utils';

const playedGameSchema = new Schema ({
  created: Date,
  updated: Date,
  game: {
    type: Schema.Types.ObjectId,
    ref: 'game'
  },
  creator: {
    id: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    },
    score: Number
  },
  partners: [{
    id: {
      type: Schema.Types.ObjectId,
      ref: 'user'
    },
    score: Number
  }]
});
module.exports = mongoose.model('PlayedGame', playedGameSchema);
