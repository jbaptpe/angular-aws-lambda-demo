import { mongoose, Schema } from '../utils';

const gameSchema = new Schema({
  name: String,
  description: String,
  rules: String,
  origin: String,
  created: Date,
  updated: Date
});
module.exports = mongoose.model('Game', gameSchema);
