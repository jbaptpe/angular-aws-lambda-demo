import { APIGatewayEvent, Callback, Context, Handler } from 'aws-lambda';
import { connectToDatabase, defaultResponseHeader } from './db/utils';

const User = require('./db/models/user');

export const create: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const currentDate = Date.now();
  const user = Object.assign({}, JSON.parse(event.body), { created: currentDate, updated: currentDate});

  function validateEmail(email) {
      var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      return re.test(String(email).toLowerCase());
  }

  if (!validateEmail(user.email)) {
    return cb(null, {
      statusCode: 400,
      headers: Object.assign({
        'Content-Type': 'text/html'
      }, defaultResponseHeader),
      body: "email is invalid"
    })
  }

  connectToDatabase().then(() => {
    User.find({ $or: [ {email: user.email}, { preferred_username: user.preferred_username} ]}).limit(1)
      .then(user => {
        if (Object.keys(user).length > 0) {
          const errMessage = user.email === user.email ? 'email exists' : 'username exists';
          return cb(null, {
            statusCode: 400,
            headers: Object.assign({
              'Content-Type': 'text/html'
            }, defaultResponseHeader),
            body: errMessage
          })
        }
      });

    User.create(user)
      .then(user => cb(null, {
        statusCode: 200,
        headers: defaultResponseHeader,
        body: JSON.stringify(user)
      }))
      .catch(err => cb(null, {
        statusCode: err.statusCode || 500,
        headers: { 'Content-Type': 'text/plain' },
        body: err
      }));
  });
};

export const get: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const params: any = event.queryStringParameters;
  let usersInfos: Array<object> = [];

  function getUsersNamesID (users) {
    users.forEach(element => {
      usersInfos.push({
        name: element.preferred_username,
        id: element._id
      });
    });
  }

  connectToDatabase().then(() => {
    if (params.type === 'getUsernamesCollection' ) {
      User.find({ preferred_username: { $regex: params.regex, $options: 'i' } }).limit(3)
          .then(users => {
            getUsersNamesID(users);

            cb(null, {
              statusCode: 200,
              headers: defaultResponseHeader,
              body: JSON.stringify(usersInfos)
            })
          })
          .catch(err => cb(null, {
            statusCode: err.statusCode || 500,
            headers: { 'Content-Type': 'text/plain' },
            body: 'Could not fetch the users.'
          }));
    } else if (params.type === 'getSingleUser') {
      User.find({ preferred_username: params.preferred_username })
        .then(user => {
          cb(null, {
          statusCode: 200,
          headers: defaultResponseHeader,
          body: JSON.stringify(user)
        })})
        .catch(err => {
          cb(null, {
          statusCode: err.statusCode || 500,
          headers: { 'Content-Type': 'text/plain' },
          body: err
        })});
    }
  });
};
