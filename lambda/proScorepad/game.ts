import { APIGatewayEvent, Callback, Context, Handler } from 'aws-lambda';
import { connectToDatabase, defaultResponseHeader } from './db/utils';

const Game = require('./db/models/game');

export const create: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
  interface Game {
    name: string,
    rules: string,
    description: string,
    origin: string
  }

  let body: Game = JSON.parse(event.body);
  body.name = body.name.toLowerCase();

  context.callbackWaitsForEmptyEventLoop = false;
  const currentDate = Date.now();
  const game = Object.assign({}, body, { created: currentDate, updated: currentDate });

  connectToDatabase().then(async () => {
    let gameExist = false;

    await Game.find({ name: body.name})
      .then(game => {
        if(Object.keys(game).length > 0) {
          gameExist = true;

          return cb(null, {
            statusCode: 400,
            headers: Object.assign({
              'Content-Type': 'text/html'
            }, defaultResponseHeader),
            body: 'A game with this name already exists'
          })
        }
      })

      if (!gameExist) {
        Game.create(game)
          .then(game => cb(null, {
            statusCode: 200,
            headers: defaultResponseHeader,
            body: JSON.stringify(game)
          }))
          .catch(err => console.log(err));
      }
  });
};

export const get: Handler = (event: APIGatewayEvent, context: Context, cb: Callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const params: any = event.queryStringParameters;
  let gamesInfos: Array<object> = [];

  function getGamesNamesId (games) {
    games.forEach(element => {
      gamesInfos.push({
        name: element.name,
        id: element._id
      });
    });
  }

  connectToDatabase()
    .then(() => {
      if(params.type === 'getSuggestions') {
        Game.find({ name: { $regex: params.regex, $options: 'i' } }).limit(3)
          .then(games => {
            getGamesNamesId(games);

            cb(null, {
              statusCode: 200,
              headers: defaultResponseHeader,
              body: JSON.stringify(gamesInfos)
            })
          })
          .catch(err => cb(null, {
            statusCode: err.statusCode || 500,
            headers: { 'Content-Type': 'text/plain' },
            body: 'Could not get the games suggestions list.'
          }));
      } else if (params.type === 'getNames') {
        let totalGames;
        Game.count()
          .then(total => totalGames = total)
          .then(() => {
            let getNamesResponse = {
              gamesTotal: totalGames
            }

            const games = params.name ? Game.find({ name: { $regex: params.name, $options: 'i' } })
                                      : Game.find()

            games.limit(parseInt(params.limit))
              .skip(parseInt(params.skip))
              .then(games => {
                cb(null, {
                  statusCode: 200,
                  headers: defaultResponseHeader,
                  body: JSON.stringify(Object.assign(getNamesResponse, { games }))
                })
              })
              .catch(err => cb(null, {
                statusCode: err.statusCode || 500,
                headers: { 'Content-Type': 'text/plain' },
                body: 'Could not fetch the games.'
              }));
          });
      } else if (params.type === 'getSingleGame') {
        Game.find({ name: params.name.toLowerCase() })
          .then(game => cb(null, {
            statusCode: 200,
            headers: defaultResponseHeader,
            body: JSON.stringify(game)
          }))
          .catch(err => cb(null, {
            statusCode: err.statusCode || 500,
            headers: { 'Content-Type': 'text/plain' },
            body: 'The game does not exist.'
          }));
      }
    });
};
